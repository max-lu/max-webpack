module.exports = {
    plugins: [
        require("autoprefixer"), // 兼容性，基于browserslist
        require("cssnano"), // 压缩
    ]
}