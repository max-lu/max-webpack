// less-loader 将less转成css
const less = require("less"); // 利用less

module.exports = function(source) {
    less.render(source, (err, res) => {
        this.callback(err, res.css);
    });
}