# 自定义loader

## loader结构

 + loader本质就是一个函数，但是不可以是箭头函数。因为在后面需要使用到loader的api。而api的访问都是通过this.xxx获取的
 + loader必须有返回值：字符串或者buffer（强硬要求）

## 如何接受配置

+ 通过loader的接口api：https://webpack.docschina.org/api/loaders/#the-loader-context

+ 通过this.query获取到options这个对象

```javascript
function(source) {
  console.info(this.query) // 打印options的配置
  return source.replace("webpack", this.query.name)
}
```

## 如何返回多个信息

+ 利用this.callback

+ https://webpack.docschina.org/api/loaders/#thiscallback

```javascript
function(source) {
  const info = source.replace("webpack", this.query.name);
  this.callback( // 同步调用
  	null,       // err
    info,       // content
  );
}
```

## 异步逻辑处理

+ 利用this.async

+ https://webpack.docschina.org/api/loaders/#thisasync

```javascript
const callback = this.async();
setTimeout(() => {
	callback(null, '');
}, 1000);
```

## 多个loader，如何配合

+ 后执行的loader函数，接收到的source是前面处理完的结果















