// 在header标签内部，生成style标签
// 接收css
module.exports = function(source) {
    return `
        const styleTag = document.createElement("style");
        styleTag.innerHTML = ${source};
        document.head.appendChild(styleTag);
    `;
}