/**
 * 自定义的loader
 * 通过loader的api，获取到一些配置，如：this.query获取到options
 * @author max-lu
 * @param {*} source test匹配到文件后，接收到的文件内容
 * @return {*} string | buffer
 */
module.exports = function(source) {
    // ------------------调用loader接口参数---------------------------
    // console.info(this.query);
    // ----------------------直接返回-----------------------
    // return source.replace("webpack", this.query.name);
    // --------------------通过callback返回多个信息-------------------------
    // const info = source.replace("webpack", this.query.name);
    // this.callback(null, info); // 同步调用
    // --------------------异步-------------------------
    // const info = source.replace("webpack", this.query.name);
    // const callback = this.async();
    // setTimeout(() => {
    //     callback(null, info)
    // }, 500);
    return source.replace("hello", "my...");
}