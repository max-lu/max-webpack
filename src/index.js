// import css from "./style/index.css"; // css-loader
// import "./style/index.less"; // 测试less-loader
// import React from "react"; // 测试bable-loader
// import ReactDOM from "react-dom";
// // import "./style/index.scss"; // sass-loader
// import { str } from "./a"; // 测试tree-shaking
// import B from "./b"; // 和c.js结合，测试react-refresh
// import C from "./c";
// import _ from "lodash"; // 测试代码分离

// import pic from "./images/webpack.jpg"; // 需要loader支持，什么loader
// var img = new Image();
// img.src = pic;
// const app = document.getElementById("app");
// app.append(img);
// console.info(222222)
// console.info("hello webpack");
// class App extends React.Component {
//   render() {
//     return (
//       <>
//         <div>hello React {str}</div>
//         <B/>
//         <C/>
//       </>
//     )
//   }
// }

// ReactDOM.render(<App/>, document.getElementById("app"))

// console.log(
//   _.join(['index', 'module', 'loaded!'], ' ')
// );


// --------------------------代码分离-动态引入 start--------------------------------------
/**
 * 动态导入：代码分离的一种方式
 * https://v4.webpack.docschina.org/guides/code-splitting/#%E5%8A%A8%E6%80%81%E5%AF%BC%E5%85%A5-dynamic-imports-
*/
// function getComponent() {
//   return import(/* webpackChunkName: "lodash" */ 'lodash').then(({ default: _ }) => {
//     var element = document.createElement('div');
//     element.innerHTML = _.join(['Hello', 'webpack'], ' ');
//     return element;
//   }).catch(error => '123')
// }
//  getComponent().then(component => {
//   document.body.appendChild(component);
// })
// --------------------------代码分离-动态引入 start--------------------------------------


// --------------------------懒加载 start--------------------------------------
import _ from "lodash";
function component() {
  var element = document.createElement('div');
  var button = document.createElement("button");
  var br = document.createElement("br");
  button.innerHTML = "click me and loog at the console";
  element.innerHTML = _.join(['Hello', 'webpack'], ' ');
  element.appendChild(br);
  element.appendChild(button);

  // Note that because a network request is involved, some indication
  // of loading would need to be shown in a production-level site/app.
  button.onclick = e => import(/* webpackChunkName: "print" */ './print').then(module => {
    var print = module.default;

    print();
  });

  return element;
}
document.body.appendChild(component());
// --------------------------懒加载 end----------------------------------------