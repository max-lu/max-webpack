import React from "react";

class C extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      count: 1,
    }
  }
  render() {
    return (
      <button onClick={() => {
        this.setState(preState => ({
          count: preState.count + 1,
        }))
      }}>{this.state.count}</button>
    )
  }
}

export default C;