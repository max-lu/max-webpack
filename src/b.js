import React from "react";

class B extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      show: true,
    }
  }
  render() {
    return (
      <button onClick={() => {
        this.setState(preState => ({
          show: !preState.show,
        }))
      }}>{this.state.show ? "是12" : "否1"}</button>
    )
  }
}

export default B;