// 当使用 useBuiltIns: "usage"的时候，不需要手动引入
// import("@babel/polyfill");
// import "@babel/polyfill";
import _ from "lodash";
// const arr = [new Promise(() => {})]

// arr.map((item) => {
//   console.info(item)
// })

console.log(
  _.join(['Another', 'module', 'loaded!'], ' ')
);