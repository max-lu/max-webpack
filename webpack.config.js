/**
 * webpack配置
 */
const path = require("path");
const minicss = require("mini-css-extract-plugin"); // css打包成文件
const { CleanWebpackPlugin } = require("clean-webpack-plugin"); // 清空目录
const htmlWebpackPlugin = require("html-webpack-plugin"); // 生产html
const maxPlugin = require("./myPlugins/max-webpack-plugin"); // 自定义plugin
const { WebpackManifestPlugin } = require('webpack-manifest-plugin'); // 打包输入映射表
const webpack = require('webpack');
const ReactRefreshPlugin = require('@pmmmwh/react-refresh-webpack-plugin');

// 基于node，所以要导出
module.exports = {
  // entry 入口:string array object
  entry: "./src/index.js", // 单入口
  // entry: { // 多入口对应多出口
    // index login可以理解为chunkName,chunks的名称
    // index: "./src/index.js",
    // login: "./src/login.js"
  // },
  // output 出口。是一个对象
  output: {
    // 本地打包产出的模块报错在哪里？默认配置叫：dist
    path: path.resolve(__dirname, "./build"), // 需要使用绝对路径
    // 本次打包产出的模块，叫什么？默认配置叫：main.js
    // filename: "main.js",
    filename: "[name]-[hash:6].js", // 多入口对应多出口，据需要使用到“占位符”placeholder [name].这个name就是上面entry的key
    // filename: "[name]-[chunkhash:6].js", // 多入口对应多出口，据需要使用到“占位符”placeholder [name].这个name就是上面entry的key
  },
  // mode 模式：打包模式：producent（生产） | development（开发） | none(不开启任何模式) 
  mode: "development",
  // mode: "production",
  // optimization: {
  //   usedExports: true, // 没有使用到的模块，在打包后，会被标记出来
  // },
  devtool: 'source-map',
  devServer: {
    // contentBase: path.join(__dirname, 'dist'),
    port: 9000,
    hot: true,
  },
  resolveLoader: {
    modules: ["./node_modules", "./myLoaders"] // 指定使用loader的时候，去哪里找
  },
  // 指定第三方
  module: {
    rules: [ // 规则列表
      {
        test: /\.css$/,
        // use: "css-loader", // 单个loader
        use: ["style-loader", "css-loader"], // 多个loader作用于1个模块，是有执行顺序的，自后往前
      },
      // {
      //   test: /\.less$/,
      //   use: ["style-loader", "css-loader", "less-loader"],
      // },
      // postcss-loader。配合postcss.config.js文件
      // {
      //   test: /\.less$/,
      //   use: ["style-loader", "css-loader", "postcss-loader", "less-loader"],
      // },
      // minicss把样式抽离成css文件
      {
        test: /\.less$/,
        use: [minicss.loader, "css-loader", "postcss-loader", "less-loader"],
      },
      // 自定义loader
      // {
      //   test: /\.less$/,
      //   use: ["max-style-loader", "max-css-loader", "max-less-loader"], // 可以这样直接使用，是resolveLoader的功劳
      // },
      {
        test: /\.scss$/,
        use: ["style-loader", "css-loader", "sass-loader"],
      },
      // 基于scss的基础上，对style-loader进行一些配置。传入对象
      // {
      //   test: /\.scss$/,
      //   use: [{
      //     loader: "style-loader",
      //     options: {
      //       injectType: "linkTag",
      //     }
      //   }, "css-loader", "sass-loader"],
      // },
      // 自定义loader
      // {
      //   test: /\.js$/,
      //   use: [
      //     {
      //       loader: path.resolve(__dirname, "./myLoaders/replace-loader-async.js"),
      //       options: {
      //         name: "options-name2"
      //       },
      //     },
      //     {
      //       loader: path.resolve(__dirname, "./myLoaders/replace-loader.js"),
      //       options: {
      //         name: "options-name2"
      //       },
      //     },
      //   ]
      // },
      // file-loader最简单配置
      // {
      //   test: /\.jpg$/,
      //   use: ["file-loader"]
      // }
      // file-loader
      // {
      //   test: /\.(png|jpe?g)|gif|webp$/,
      //   use: [
      //     {
      //       loader: "file-loader",
      //       options: {
      //         name: "[name].[ext]", // 占位符：[ext]表示后缀
      //         outputPath: "images", // 图片的存储位置
      //         publicPath: "../images", // 图片的引入地址
      //       }
      //     }
      //   ]
      // },
      // url-loader、图片压缩(image-webpack-loader)
      {
        test: /\.(png|jpe?g)|gif|webp$/,
        use: [
          {
            loader: "url-loader",
            options: {
              name: "[name]-[hash:6].[ext]", // 占位符：[ext]表示后缀
              outputPath: "images", // 图片的存储位置
              publicPath: "../images", // 图片的引入地址
              limit: 3 * 1024, // 小于N Kb的图片，都会转成base64.如果不配置，所有的都会转成base64
            }
          },
          {
            loader: "image-webpack-loader",
            // options: {
            //   mozjpeg: 
            // }
          }
        ]
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        use: {
          loader: "file-loader",
          options: {
            name: "[name].[ext]",
            outputPath: "font",
            publicPath: "../font",
          }
        }
      },
      // {
      //   test: /\.js$/,
      //   use: [
      //     {
      //       loader: "babel-loader",
      //       options: {
      //         presets: ['@babel/preset-env'],
      //       }
      //     }
      //   ]
      // },
      // 设置按需加载
      {
          test: /\.js$/,
          use: [
            {
              loader: "babel-loader",
              options: {
                presets: [
                  // [
                  //   '@babel/preset-env',
                  //   {
                  //     targets: {
                  //       edge: "17",
                  //     },
                  //     corejs: 2,
                  //     useBuiltIns: "usage",
                  //   },
                  // ],
                  "@babel/preset-react",
                ],
                plugins: [require.resolve('react-refresh/babel')].filter(Boolean),
              },
            }
          ]
        }
    ]
  },
  plugins: [
    new ReactRefreshPlugin(), // react热更新（不影响state）
    new minicss({ // 样式文件表
      filename: "style/index-[contenthash:6].css"
    }),
    new CleanWebpackPlugin(), // 清空目录
    new htmlWebpackPlugin({ // 创建html文件
      template: "./public/index.html",
      filename: "index.html",
      chunks: ["main"],
    }),
    // new htmlWebpackPlugin({
    //   template: "./public/login.html",
    //   filename: "login.html",
    //   chunks: ["login"],
    // }),
    // new maxPlugin({
    //   name: "my-plugin-name"
    // }),
    new WebpackManifestPlugin(), // manifest 打包对应表，没啥用
    // new webpack.HotModuleReplacementPlugin(), // webpack自带的热更新
  ]
}