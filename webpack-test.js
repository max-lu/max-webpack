/** webpack 如何获得“订单”、开始“加工”等操作
 * 
  1.我有一个汽水代工厂（没有自己的产品，全部都是外部订单，供应工厂：webpack）

  2.订单：按照订单要求生产汽水（口味、包装、颜色），理解为：webpack.config.js

  3.流水线操作：30条流水线作业（原材料从第一条流水线开始，到最后一条。就是客户要求的汽水产品）。流水线操作对应着compiler.hook

  4.流水线监控：每一条流水线都有相应的检测机制，通过这个机制，可以查看当前流水线的产品信息。对应着compilations

  通过执行 node webpack-test.js
 * 
 */
const webpack = require("webpack"); // 工厂

const config = require("./webpack.config.js"); // 订单

const compiler = webpack(config); // 流水线

Object.keys(compiler.hooks).forEach(hookName => {
    // 这里注册了一个执行每一个钩子的时候，打印hookName
    compiler.hooks[hookName].tap("事件名称", (compilition) => {
        console.info(`run -----> ${hookName}`);
    })
})

compiler.run(); // 通知开始干活