/** webpack 开发配置 */

const { merge } = require("webpack-merge");
const common = require("./webpack.common");
const ReactRefreshPlugin = require('@pmmmwh/react-refresh-webpack-plugin');
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

module.exports = merge(common, {
  mode: "development",
  devtool: "source-map",
  devServer: {
    port: 9001,
    hot: true,
  },
  plugins: [
    new CleanWebpackPlugin(),
    new ReactRefreshPlugin(),
  ]
})