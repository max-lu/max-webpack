/** webpack通用配置 */

const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: {
    index: "./src/index.js",
    // another: "./src/login.js",
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: '[name].bundle.js',
    chunkFilename: '[name].bundle.js',
  },
  optimization: {
    splitChunks: { // 避免重复，将公共的依赖模块提取到已有的entery chunk中。或者提取到一个新生成的chunk(推荐)
      chunks: 'all'
    }
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.less$/,
        use: ["style-loader", "css-loader", "postcss-loader", "less-loader"],
      },
      {
        test: /\.scss$/,
        use: ["style-loader", "css-loader", "sass-loader"],
      },
      {
        test: /\.(png|jpe?g)|gif|webp$/,
        use: [
          {
            loader: "url-loader",
            options: {
              name: "[name]-[hash:6].[ext]",
              outputPath: "images",
              publicPath: "../images",
              limit: 3 * 1024,
            }
          },
          {
            loader: "image-webpack-loader",
          }
        ]
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        use: {
          loader: "file-loader",
          options: {
            name: "[name].[ext]",
            outputPath: "font",
            publicPath: "../font",
          }
        }
      },
      {
        test: /\.(js|mjs|jsx|ts|tsx)$/,
        use: [
          {
            loader: "babel-loader",
            options: {
              presets: [
                "@babel/preset-react",
              ],
              plugins: [require.resolve('react-refresh/babel')].filter(Boolean),
            },
          }
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "hello webpack",
      template: "./public/index.html",
    })
  ]
}