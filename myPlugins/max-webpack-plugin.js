/**
 * plugin的结构：class
 * 1.接受options参数，在constructor里面获取
 * 2.必须有一个apply方法。插件具体做什么，都在这个方法里
 */

class MaxPlugin {
    constructor(options) {
        console.info(options.name)
    }
    /** 插件必须要有apply函数
     * compiler：webpack执行打包时生成的对象。webpack的实例化
     */
    apply(compiler) {
        // compiler.hooks 对应webpack的打包生命周期
        // 同步
        compiler.hooks.done.tap("事件名称", (compilation) => { // compilation:当前hook对应的状态
            // 事件内容
            console.info("111111")
         })
         // 异步
         compiler.hooks.emit.tapAsync("事件名称", (compilation, cb) => {
            // 事件内容
           // 异步钩子执行完成后，不要忘记执行回调函数！！！
           console.info(compilation.assets)
           compilation.assets["max.txt"] = {
               source: () => {
                    return "这里是max的内容"
               },
               size() {
                   return 123;
               }
           }
           cb();
         })
    }

}

module.exports = MaxPlugin;